// This code is mainly checking for the navigation options.

let divider = document.querySelector('#divider');
let navItem1 = document.querySelector('#navSessionBtn1');
let navItem2 = document.querySelector('#navSessionBtn2');

let userToken = localStorage.getItem("token");


// This checks if the user is logged in.
if (!userToken) {
	
	divider.innerHTML = 

	`
	<hr>
	`
	// This checks if the user is in the register page through index.
	if (window.location.href.includes("pages")) { 

		// This checks if the user is in the register page directly.
		if (window.location.href.includes("register")) {
			navItem1.innerHTML =
			`
			<li class="nav-item">
			<a href="./register.html" class="nav-link active"> Register </a>
			</li>
			`
			navItem2.innerHTML =
			`
			<li class="nav-item">
			<a href="./login.html" class="nav-link"> Login </a>
			</li>
			`
		}
		else if (window.location.href.includes("courses")) {
  			navItem1.innerHTML =
			`
			<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
			</li>
			`
		}

		}
	// If not, the register button on the navbar is not active.
	else {

		navItem1.innerHTML =
		`
		<li class="nav-item">
		<a href="pages/register.html" class="nav-link"> Register </a>
		</li>
		`
		navItem2.innerHTML =
		`
		<li class="nav-item">
		<a href="pages/register.html" class="nav-link"> Register </a>
		</li>
		`
		
	}

	// This checks if the user is in the login page through index.
	if (window.location.href.includes("pages")) {
		// This checks if the user is in the login page directly.
		if (window.location.href.includes("login")) {
			navItem1.innerHTML =
			`
			<li class="nav-item">
			<a href="register.html" class="nav-link "> Register </a>
			</li>
			`
			navItem2.innerHTML =
			`
			<li class="nav-item">
			<a href="login.html" class="nav-link active"> Login </a>
			</li>
			`
		}
		else {
			navItem2.innerHTML =
			`
			<li class="nav-item">
			<a href="login.html" class="nav-link"> Login </a>
			</li>
			`
		}
	}
	// If not, the login button on the navbar is not active.
	else {
		navItem2.innerHTML =
		`
		<li class="nav-item">
		<a href="pages/login.html" class="nav-link"> Login </a>
		</li>
		`
	}

}

// This is if the user is logged in.
else {
	divider.innerHTML = 

	`
	<hr>
	`

	// This checks if the user is in the login page through the pages.
	if (window.location.href.includes("pages")) {
		if (window.location.href.includes("profile")) {

			navItem1.innerHTML =
			`
			<li class="nav-item">
			<a href="./profile.html" class="nav-link active"> My Profile </a>
			</li>
			`

			navItem2.innerHTML =
			`
			<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
			`
		}
		else if (window.location.href.includes("editProfile")) {

			navItem1.innerHTML =
			`
			<li class="nav-item">
			<a href="./profile.html" class="nav-link active"> My Profile </a>
			</li>
			`

			navItem2.innerHTML =
			`
			<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
			`
		}
		else if (window.location.href.includes("course")) {
			navItem1.innerHTML =
			`
			<li class="nav-item">
			<a href="./profile.html" class="nav-link"> My Profile </a>
			</li>
			`
			navItem2.innerHTML =
			`
			<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
			`
		}
		else {
			navItem1.innerHTML =
			`
			<li class="nav-item">
			<a href="./profile.html" class="nav-link"> My Profile </a>
			</li>
			`
			navItem2.innerHTML =
			`
			<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
			`
		}
	}
	// If not, the login button on the navbar is not active.
	else {

		navItem1.innerHTML =
		`
		<li class="nav-item">
		<a href="pages/profile.html" class="nav-link"> My Profile </a>
		</li>
		`
		navItem2.innerHTML =
		`
		<li class="nav-item">
		<a href="pages/logout.html" class="nav-link"> Logout </a>
		</li>
		`
	}

}