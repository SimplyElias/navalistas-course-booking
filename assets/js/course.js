// // This returns the query
// console.log(window.location.search);

// This gets the params of the course ID. I think...
let params = new URLSearchParams(window.location.search);
// This gets the entire token information of the user.
let token = localStorage.getItem('token');
let userId = localStorage.getItem('id');
let adminUser = localStorage.getItem("isAdmin");

// The has method checks if the "courseId" key exists in the URL query string
// The method returns true if key exists

console.log(params.has('courseId'));

let courseId = params.get('courseId');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let courseStatus = document.querySelector("#courseStatus");

let enrolledUsersHeader = document.querySelector('#enrolledUsersHeader')
let enrollContainer = document.querySelector("#enrollButton");
let adminContainer = document.querySelector("#adminButtons");
let coursePageFooter = document.querySelector("#coursePageFooter")
let enrolledCourseUsers = new Array()

if (params == "") {
	alert("No course selected!")
	window.location.replace("./courses.html")
}
else {
	fetch(`https://lit-island-71561.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		let enrolledUsers = document.querySelector("#enrolledUsers");
		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;

		data.enrollees.map(users => {
			enrolledCourseUsers.push(users.userId)
		})

		if (enrolledCourseUsers.includes(userId)){
			enrollContainer.innerHTML =
			`
			<button id="enrollButton" class="btn btn-block btn-success">
			Enrolled!
			</button>
			`
		}
		else {
			enrollContainer.innerHTML =
			`
			<button id="enrollButton" class="btn btn-block btn-primary">
			Enroll
			</button>
			`
		}

		if (adminUser === "true") {
			adminContainer.innerHTML =
			`
			<br>
			<button id="editButton" onclick="window.location.href='./editCourse.html?courseId=${courseId}'" class="btn btn-block btn-info">
			Edit Course
			</button>
			`
			if (data.isActive == true) {
				coursePageFooter.innerHTML =
				`
				<footer class="footer">
				<p class="p-3 mb-0 text-center text-black"> Navalistas Booking Services | <a href="https://twitter.com/navalistatic" target="_blank">Von Naval </a> &copy; 2021</p>
				<div class="footer-icons">
				<a href="https://facebook.com/lyraotic" target="_blank"><i class="fb-icon fab fa-facebook-square"></i></a>
				<a href="https://www.youtube.com/channel/UCzirh0d5I7kLl2q39HPjlag" target="_blank"><i id="yt-icon" class="fab fa-youtube"></i></a>
				<a href="https://twitch.tv/elias_osu" target="_blank"><i id="twitch-icon" class="fab fa-twitch"></i></a>
				<a href="https://www.linkedin.com/in/von-aubrey-naval/" target="_blank"><i id="linkedin-icon" class="fab fa-linkedin"></i></a>
				<a href="https://www.instagram.com/navalistatic/" target="_blank"><i id="ig-icon" class="fab fa-instagram"></i></a>
				<a href="https://www.twitter.com/navalistatic" target="_blank"><i id="twt-icon" class="fab fa-twitter"></i></a>
				</div>
				</footer>
				`
				courseStatus.innerHTML =
				`
				<h6> Status: <strong class="text-success">Enabled</strong> </h6>
				`
				adminContainer.innerHTML +=
				`
				<button id="enrollButton" onclick="window.location.href='./deleteCourse.html?courseId=${courseId}'" class="btn btn-block btn-danger">
				Disable Course
				</button>
				`
			}
			else {
				coursePageFooter.innerHTML =
				`
				<footer class="footer fixed-bottom">
				<p class="p-3 mb-0 text-center text-black"> Navalistas Booking Services | <a href="https://twitter.com/navalistatic" target="_blank">Von Naval </a> &copy; 2021</p>
				<div class="footer-icons">
				<a href="https://facebook.com/lyraotic" target="_blank"><i class="fb-icon fab fa-facebook-square"></i></a>
				<a href="https://www.youtube.com/channel/UCzirh0d5I7kLl2q39HPjlag" target="_blank"><i id="yt-icon" class="fab fa-youtube"></i></a>
				<a href="https://twitch.tv/elias_osu" target="_blank"><i id="twitch-icon" class="fab fa-twitch"></i></a>
				<a href="https://www.linkedin.com/in/von-aubrey-naval/" target="_blank"><i id="linkedin-icon" class="fab fa-linkedin"></i></a>
				<a href="https://www.instagram.com/navalistatic/" target="_blank"><i id="ig-icon" class="fab fa-instagram"></i></a>
				<a href="https://www.twitter.com/navalistatic" target="_blank"><i id="twt-icon" class="fab fa-twitter"></i></a>
				</div>
				</footer>
				`
				courseStatus.innerHTML =
				`
				<h6> Status: <strong class="text-danger">Disabled</strong> </h6>
				`
				adminContainer.innerHTML +=
				`
				<button id="enrollButton" onclick="window.location.href='./enableCourse.html?courseId=${courseId}'" class="btn btn-block btn-success">
				Enable Course
				</button>
				`
			}

			enrolledUsersHeader.innerHTML +=
			`<h2> Enrolled Users </h2>
			`

			if (data.enrollees < 1) {
				enrolledUsers.innerHTML = "<h6>There are no users enrolled this course!</h6>"
			}
			else {

				enrolledUsers.innerHTML =
				`
				<tr>
				<th> Full Name </th>
				<th> User Id </th>
				<th> Enrolled On </th>
				</th>
				</tr>
				<tbody id="enrollees"></tbody>
				`
			}

			let enrollees = document.querySelector("#enrollees");


			data.enrollees.map(enrolledData => {

				fetch(`https://lit-island-71561.herokuapp.com/api/users/details/${enrolledData.userId}`, {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					}
				})
				.then(res => res.json())
				.then(data => {


					enrollees.innerHTML +=
					`
					<tr>
					<td>
					<h6 class="">
					${data.firstName} ${data.lastName}
					</h6>
					</td>
					<td>
					<p class="">
					${data._id}
					</p>
					</td>
					<td>
					<p class="">
					${enrolledData.enrolledOn}
					</p>
					</td>
					</tr>
					`


				})

			})


		}
	})


document.querySelector("#enrollButton").addEventListener("click", () => {

	if (enrolledCourseUsers.includes(userId)){
		alert("You're already enrolled to this course! You can't enroll again!")
	}
	else {
		fetch('https://lit-island-71561.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {
				alert("Enrolled! See you in class!")
				window.location.replace("./courses.html")
			}
			else {
				alert("Something went wrong.")
			}
		})
	}
})}


