// Selects the register form
let registerForm = document.querySelector("#registerUser");

// Adds an event to the register form
registerForm.addEventListener("submit", (e) => {

	e.preventDefault();


	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	// Start of Name validation
	console.log(/\d/.test(firstName))
	console.log(/\d/.test(lastName))
	if (firstName === "") {
		document.querySelector("#nameErr").innerHTML = `<label class="text-danger">Your name inputs cannot be empty!</label>`
	}
	if (lastName === "") {
		document.querySelector("#nameErr").innerHTML = `<label class="text-danger">Your name inputs cannot be empty!</label>`
	}
	if (firstName !== "") {
		if (/\d/.test(firstName) === true) {
			document.querySelector("#nameErr").innerHTML = `<label class="text-danger">Please recheck your name inputs!</label>`
		}
		else {
			document.querySelector("#nameErr").innerHTML = ``
		}
	}
	if (lastName !== "") {
		if (/\d/.test(lastName) === true) {
			document.querySelector("#nameErr").innerHTML = `<label class="text-danger">Please recheck your name inputs!</label>`
		}
		else {
			document.querySelector("#nameErr").innerHTML = ``
		}
	}
	// End of name validation

	// Pasword validation
	if (password1 !== password2) {
		document.querySelector("#passwordErr").innerHTML = `<label class="text-danger">Your passwords don't match!</label>`
	}
	if (password1 === password2) {
		if (password1 == "" || password2 == "") {
			document.querySelector("#passwordErr").innerHTML = `<label class="text-danger">Your passwords cannot be empty!</label>`
		}
		else {
			document.querySelector("#passwordErr").innerHTML = ``
		}
	}
	// End of password validation

	// Start of mobile number validation
	if (mobileNumber.length < 11 || mobileNumber.length > 11) {
		document.querySelector("#mobileNumErr").innerHTML = `<label class="text-danger">Your mobile number is invalid!</label>`
	}
	if (mobileNumber.length === 11) {
		document.querySelector("#mobileNumErr").innerHTML = ``
	}
	// End of mobile number validation

	//Start of email validation
	if (email === "") {
		document.querySelector("#userEmailErr").innerHTML = `<label class="text-danger">Your email cannot be empty!</label>`
	}
	if (email !== "") {
		document.querySelector("#userEmailErr").innerHTML = ``
	}
	// End of email validation

	if ((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11) && (firstName !== "") && (lastName !== "")) {
		fetch('https://lit-island-71561.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				"email": email
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === false) {

				fetch('https://lit-island-71561.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if (data === true) {
						alert("Registered successfully!");

						window.location.replace("./login.html");
					}
					else {
						alert("Something went wrong.")
					}
				})
			}

			else {
				alert("This registered email address already exists!")
				document.querySelector("#userEmailErr").innerHTML = `<label class="text-danger">This email already exists!"</label>`
			}
		})
	}
})