
let params = new URLSearchParams(window.location.search);

let token = localStorage.getItem('token');

let profileContainer = document.querySelector("#profileContainer");

let coursesDisplay = document.querySelector("#enrolledCourses")

if(!token || token === null){

	alert('You must login first.');
	window.location.replace("./login.html");

}
else {
	fetch('https://lit-island-71561.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {

		document.getElementById('firstName').innerHTML = data.firstName;
		document.getElementById('lastName').innerHTML = data.lastName;
		document.getElementById('userEmail').innerHTML = data.email;
		document.getElementById('mobileNum').innerHTML = data.mobileNo;

		coursesDisplay.innerHTML =
		`
		<tr>
		<th> Course Name </th>
		<th> Course Status </th>
		<th> Enrolled On </th>
		</th>
		</tr>
		<tbody id="enrollments"></tbody>
		`

		//  This really doesn't work.
		// for (var j = 0; j < data.enrollments.length; j++){
		// 	enrolledCourses[j] = data.enrollments[j].courseId
		// 	datedCourses[j] = data.enrollments[j].enrolledOn
		// 	console.log("datedCourse Index: " + j + " | value: " + datedCourses[j])
		// }
		data.enrollments.map(courseData => {

			fetch(`https://lit-island-71561.herokuapp.com/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {
				let enrolledData = document.querySelector('#enrollments');

				if(data.isActive === true) {
					enrolledData.innerHTML +=
					`
					<td>
					<h5>
					${data.name}
					</h5>
					</td>
					<td>
					<p>
					${courseData.status}
					</p>
					</td>
					<td>
					<p class="text-left">
					${courseData.enrolledOn}
					</p>
					</td>
					`
				}
			})
		})
		if (data.enrollments < 1) {
			document.querySelector("#enrolledCourses").innerHTML = "You have no courses enrolled onto."
		}
	})
}

	// 		let courseData;
	// 	// This loops through the array enrolledCourses or datedCourses
	// 	let z = 0;
	// 	courseData = data.map(course => {

	// 		activeCourses.push(course._id)
	// 		console.log(enrolledCourses.includes(activeCourses[z]))
	// 		if(enrolledCourses.includes(activeCourses[z])) {
	// 			if(enrolledCourses.includes(course._id)) {

	// 			}
	// 		}
	// 		z++;
	// 	})
	// })
	
		// for (let x = 0; x < data.enrollments.length; x++){
		// console.log("The for loop is now at " + x + " which the courseId is " + data.enrollments[x].courseId)
		
		 // Promise.all( 'fetch(`https://lit-island-71561.herokuapp.com/api/courses/${data.enrollments[x].courseId}`)')
		 // .then(res => res.json())
		 // .then(data => {

	//This checks if the user has been enrolled to any courses.




