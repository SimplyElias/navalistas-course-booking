// Selects the register form
let loginForm = document.querySelector("#logInUser");

// Adds an event to the register form
loginForm.addEventListener("submit", (e) => {

	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	console.log(email);
	console.log(password);

	if (email === "") {
		document.querySelector("#userEmailErr").innerHTML = `<label class="text-danger"> Your email cannot be blank! </label>`
	}
	if (password === "") {
		document.querySelector("#passwordErr").innerHTML = `<label class="text-danger"> Your password cannot be blank! </label>`
	}
	if (email !== "") {
		document.querySelector("#userEmailErr").innerHTML = ``
	}
	if (password !== "") {
		document.querySelector("#passwordErr").innerHTML = ``
	}
	if (email !== "" && password !== "") {
		fetch('https://lit-island-71561.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data  => {
			
			console.log(data)

			if (data.accessToken) {


				localStorage.setItem('token', data.accessToken)

				fetch('https://lit-island-71561.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

							//Stores the user id and isAdmin properties in the local storage
							localStorage.setItem("id", data._id);
							localStorage.setItem("isAdmin", data.isAdmin);

							window.location.replace('./courses.html');
						})

			}
			else if (!data.accessToken) {
				document.querySelector("#passwordErr").innerHTML = `<label class="text-danger"> Login failed. Please check your email or password! </label>`
			}
			else {
				document.querySelector("#passwordErr").innerHTML = `<label class="text-danger"> Login failed. An error occurred! </label>`
			}
		})
	}
})