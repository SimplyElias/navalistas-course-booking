let adminUser = localStorage.getItem("isAdmin");
let formEdit = document.querySelector("#editCourse")
let token = localStorage.getItem("token");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let currCourseName

if (adminUser == "false" || !adminUser) {
	alert("You don't have administrator rights to this page!")
	window.location.replace("./courses.html")
}
else if (params == "") {
	alert("No course selected!")
	window.location.replace("./courses.html")
}
else {
	fetch(`https://lit-island-71561.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		document.getElementById('courseNameLabel').innerHTML = data.name;

		document.getElementById('courseName').value = data.name;
		document.getElementById('courseDescription').value = data.description;
		document.getElementById('coursePrice').value = data.price;
		
		document.getElementById('courseName').placeholder = data.name;
		document.getElementById('courseDescription').placeholder = data.description;
		document.getElementById('coursePrice').placeholder = data.price;

		let courseNameErr = document.querySelector("#courseNameErr")
		let courseDescErr = document.querySelector("#courseDescErr")
		let coursePriceErr = document.querySelector("#coursePriceErr")

		currCourseName = data.name
	})

	formEdit.addEventListener("submit", (e) => {


		e.preventDefault();


		let courseId = params.get('courseId');
		let courseName = document.querySelector("#courseName").value;
		let courseDesc = document.querySelector("#courseDescription").value;
		let coursePrice = document.querySelector("#coursePrice").value;

		if (courseName === "" ) {
			courseNameErr.innerHTML = `<label class="text-danger">The course name cannot be blank!</label>`
		}
		if (courseDesc === "" ) {
			courseDescErr.innerHTML = `<label class="text-danger">The course description cannot be blank!</label>`
		}
		if (coursePrice === "") {
			coursePriceErr.innerHTML = `<label class="text-danger">The course price cannot be blank!</label>`
		}
		if (courseName !== "" ) {
			courseNameErr.innerHTML = ``
		}
		if (courseDesc !== "" ) {
			courseDescErr.innerHTML = ``
		}
		if (coursePrice !== "") {
			coursePriceErr.innerHTML = ``
		}
		if (courseName !== "" && courseDesc !== "" && coursePrice !== "") {
			fetch('https://lit-island-71561.herokuapp.com/api/courses/course-exists/', {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					name: courseName.toString()
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if ((data === true) && (currCourseName !== courseName)) {
					console.log(currCourseName + "ASDAS" + courseName)
					alert(`The course named '${courseName}' already exists in the database!`)
					courseNameErr.innerHTML = `<label class="text-danger"> The course name '${courseName}' already exists! </label>`
				}
				else if ((data === true) && (currCourseName === courseName)) {
					fetch('https://lit-island-71561.herokuapp.com/api/courses', {
						method: 'PUT',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify({
							courseId: courseId,
							name: courseName,
							description: courseDesc,
							price: coursePrice
						})
					})
					.then(res => res.json())
					.then(data => {
						console.log(data)

						if (data === true) {
							alert(`You have updated the course: ${courseName}`)
							window.location.replace(`./course.html?courseId=${courseId}`)
						}
						else {
							alert(`Something went wrong...`)
						}
					})
				}
			})
		}
	})
}
