let adminUser = localStorage.getItem("isAdmin");
let token = localStorage.getItem('token');
let cardFooter;
let modalButton = document.querySelector("#adminButton");
let coursesHeader = document.querySelector("#coursesHeader")
let stautsText;

if (adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

	coursesHeader.innerHTML =
	`
	<h1>Courses</h1>
	<p class="lead"> Book your course on us today! </p>
	<a href="#coursesWindow" class="btn btn-outline-primary"> Book Now! </a>
	`
} 
else {
	coursesHeader.innerHTML =
	`
	<h1>Courses</h1>
	<p class="lead">Hello admin!</p>
	<span id="totalCourses"></span>
	`
	modalButton.innerHTML = 
	`<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-primary">
	Add Course
	</a>
	</div>`
}

if (adminUser === "true") {
	fetch(`https://lit-island-71561.herokuapp.com/api/courses/all`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		let courseData;
		

		let totalCourses = document.querySelector("#totalCourses")
		if (data.length < 1) {
			totalCourses.innerHTML =
			`
			<p> You currently have no courses in the database! </p>
			`
			courseData = "There are no courses available."
		} else {

			totalCourses.innerHTML =
			`
			The platform currently has ${data.length} courses in the database! 
			`

			courseData = data.map(course => {
				let enrolledUsers;


				if(userToken == "" || !userToken) {

					cardFooter =
					`
					<a href="./register.html" value="" class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					`
				}

				else if(adminUser == "false" || !adminUser) {

					cardFooter =
					`
					<a href="./course.html?courseId=${course._id}" value="{course._id" class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					`
				}

				else {
					if (course.isActive === true) {
						stautsText = `<span style="color: Green"><strong>Enabled</strong></span>`
						cardFooter =
						`<a href="./course.html?courseId=${course._id}" value="{course._id" class="btn btn-primary text-white btn-block editButton"> Select Course </a>
						<a href="./editCourse.html?courseId=${course._id}" value="{course._id" class="btn btn-info text-white btn-block editButton"> Edit </a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="{course._id" class="btn btn-danger text-white btn-block deleteButton"> Disable Course </a>
						`
					}
					else {
						stautsText = `<span style="color: Red"><strong>Disabled</strong></span>`
						cardFooter =
						`<a href="./course.html?courseId=${course._id}" value="{course._id" class="btn btn-primary text-white btn-block editButton"> Select Course </a>
						<a href="./editCourse.html?courseId=${course._id}" value="{course._id" class="btn btn-info text-white btn-block editButton"> Edit </a>
						<a href="./enableCourse.html?courseId=${course._id}" value="{course._id" class="btn btn-success text-white btn-block deleteButton"> Enable Course </a>
						`	
					}

				}
				return (
					`
					<div class="col-md-6 my-3">
					<div class="card">
					<div class="card-body">
					<h5 class="card-title">
					<strong>${course.name}</strong/>
					</h5>
					<p class="card-text text-left">
					${course.description}
					</p>
					<p class="card-text text-right">
					Price: <strong> ₱ ${course.price} </strong/>
					</p>
					<p class="card-text text-left">
					Status: ${stautsText}
					</p>
					<p class="card-text text-left">
					Enrolled Users: <strong>${course.enrollees.length}</strong>
					</p>
					</div>
					<div class="card-footer">
					${cardFooter}
					</div>
					</div>
					</div>
					`)
			}).join("");
		}

		document.querySelector("#coursesContainer").innerHTML = courseData;
	})
}
else {
	fetch('https://lit-island-71561.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		let courseData;

		if (data.length < 1) {

			courseData = "There are no courses available."
		} else {

			courseData = data.map(course => {

				if(userToken == "" || !userToken) {

					cardFooter =
					`
					<a href="./register.html" value="" class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					`
				}

				else if(adminUser == "false" || !adminUser) {

					cardFooter =
					`
					<a href="./course.html?courseId=${course._id}" value="{course._id" class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					`
				}
				return (
					`
					<div class="col-md-6 my-3">
					<div class="card">
					<div class="card-body">
					<h5 class="card-title">
					${course.name}
					</h5>
					<p class="card-text text-left">
					${course.description}
					</p>
					<p class="card-text text-right">
					Php ${course.price}
					</p>
					</div>
					<div class="card-footer">
					${cardFooter}
					</div>
					</div>
					</div>
					`)
			}).join("");
		}

		document.querySelector("#coursesContainer").innerHTML = courseData;
	})
}
