let formEdit = document.querySelector("#editProfileForm")
let userId = localStorage.getItem("id");
let token = localStorage.getItem("token");
let params = new URLSearchParams(window.location.search);


let currfirstName;
let currlastName;
let currEmail;
let currMobileno;

fetch('https://lit-island-71561.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	document.getElementById('firstName').value = data.firstName;
	document.getElementById('lastName').value = data.lastName;
	document.getElementById('userEmail').value = data.email;
	document.getElementById('mobileNum').value = data.mobileNo;

	document.getElementById('firstName').placeholder = data.firstName;
	document.getElementById('lastName').placeholder = data.lastName;
	document.getElementById('userEmail').placeholder = data.email;
	document.getElementById('mobileNum').placeholder = data.mobileNo;

	currfirstName = data.firstName;
	currlastName = data.lastName;
	currEmail = data.email;
	currMobileno = data.mobileNo;

})

formEdit.addEventListener("submit", (e) => {

	e.preventDefault();

	let userId = params.get('userId');
	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let email = document.querySelector("#userEmail").value;
	let mobileNumber = document.querySelector("#mobileNum").value;

	if (firstName === "") {
		document.querySelector("#firstNameErr").innerHTML = `<label class="text-danger">Your first name input cannot be empty!</label>`
	}
	if (lastName === "") {
		document.querySelector("#lastNameErr").innerHTML = `<label class="text-danger">Your last name inputs cannot be empty!</label>`
	}
	if (firstName !== "") {
		if (/\d/.test(firstName) === true) {
			document.querySelector("#firstNameErr").innerHTML = `<label class="text-danger">Please recheck your first name inputs!</label>`
		}
	}
	if (lastName !== "") {
		if (/\d/.test(lastName) === true) {
			document.querySelector("#lastNameErr").innerHTML = `<label class="text-danger">Please recheck your last name inputs!</label>`
		}
	}
	if (firstName !== "") {
		document.querySelector("#firstNameErr").innerHTML = ``
	}
	if (lastName !== "") {
		document.querySelector("#lastNameErr").innerHTML = ``
	}
	// End of name validation

	// Start of mobile number validation
	if (mobileNumber.length < 11 || mobileNumber.length > 11) {
		document.querySelector("#mobileNumErr").innerHTML = `<label class="text-danger">Your mobile number is invalid!</label>`
	}
	if (mobileNumber.length === 11) {
		document.querySelector("#mobileNumErr").innerHTML = ``
	}
	// End of mobile number validation

	//Start of email validation
	if (email === "") {
		document.querySelector("#userEmailErr").innerHTML = `<label class="text-danger">Your email cannot be empty!</label>`
	}
	if (email !== "") {
		document.querySelector("#userEmailErr").innerHTML = ``
	}
	// End of email validation

	if ((mobileNumber.length === 11) && (firstName !== "") && (lastName !== "")) {
		fetch('https://lit-island-71561.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if ((data === true) && (currEmail !== email)) {
				alert("This email address already exists in our database!")
				document.querySelector("#userEmailErr").innerHTML = "This email already exists!"
			}
			else if ((data === true) && (currEmail === email)) {
				fetch('https://lit-island-71561.herokuapp.com/api/users/edit', {
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if (data === true) {
						alert(`You have updated your profile successfully!`)
						window.location.replace("./profile.html")
					}
					else {
						alert(`Something went wrong...`)
					}
				})
			}
			else {
				fetch('https://lit-island-71561.herokuapp.com/api/users/edit', {
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if (data === true) {
						alert(`You have updated your profile successfully!`)
						window.location.replace("./profile.html")
					}
					else {
						alert(`Something went wrong...`)
					}
				})
			}
		})
	}
})


// function validateEmail(params){
// 	let paramsEmail = {
// 		email: params
// 	}
// 	fetch('https://lit-island-71561.herokuapp.com/api/users/email-exists', {
// 		method: 'POST',
// 		headers: {
// 				'Content-Type': 'application/json'
// 		},
// 		body: JSON.stringify({
// 			email: paramsEmail.email
// 		})
// 	})
// 	.then(res => res.json())
// 	.then(data => {
// 		return data
// 	})
// }