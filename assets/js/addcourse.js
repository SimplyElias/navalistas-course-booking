let formSubmit = document.querySelector("#createCourse")

let token = localStorage.getItem("token");

let courseNameErr = document.querySelector("#courseNameErr");
let courseDescErr = document.querySelector("#courseDescErr");
let coursePriceErr = document.querySelector("#coursePriceErr");

formSubmit.addEventListener("submit", (e) => {

	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	let courseDesc = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	if (courseName === "" ) {
		courseNameErr.innerHTML = `The course name cannot be blank!`
	}
	if (courseDesc === "" ) {
		courseDescErr.innerHTML = `The course description cannot be blank!`
	}
	if (coursePrice === "") {
		coursePriceErr.innerHTML = `The course price cannot be blank!`
	}
	if (courseName !== "") {
		courseNameErr.innerHTML = ``
	}
	if (courseDesc !== "") {
		courseDescErr.innerHTML = ``
	}
	if (coursePrice !== "") {
		coursePriceErr.innerHTML = ``
	}
	if (/^[a-zA-Z]+$/.test(coursePrice) === true) {
		coursePriceErr.innerHTML = `The course price is invalid!`
	}
	if (courseName !== "" && courseDesc !== "" && coursePrice !== "") {
		fetch('https://lit-island-71561.herokuapp.com/api/courses/course-exists/', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: courseName.toString()
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {
				alert(`The course named '${courseName}' already exists in the database!`)
				courseNameErr.innerHTML = `The course name '${courseName}' already exists!`
			}
			else {
				fetch('https://lit-island-71561.herokuapp.com/api/courses', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						name: courseName,
						description: courseDesc,
						price: coursePrice
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if (data === true) {
						alert(`You have added the course ${courseName}!`)
						window.location.replace("./courses.html")
					}
					else {
						alert(`Something went wrong...`)
					}
				})
			}
		})
	}
})