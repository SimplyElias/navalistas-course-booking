Push #1:
- Pushed the backend stuff.
- Added new index.html and style.css
- Added certain images.


Push #2: 
- Created Frontend Basic HTML
- Created Profile Page
- Polished Profile Page
- Fixing up nav-links in script.js
- Created multiple js in Frontend.
	- editProfile.js
	- profile.js

Push #3:
- Added the following JS:
	- enableCourse.js
- Added the following HTMLs:
	- enableCourse.html
- Updated:
	- profile.js
	- course.js
	- courses.js
	- script.js
- Modified contollers/routes for user and course

Push #4: 
- Modified contollers:
	- user.js
- Modified routes:
	- user.js
- Fixed:
	- profile.js
	- course.js
- Updated:
	- editCourse.js
- Updated HTML:
	- course.html
	- editCoruse.html

Push #5:
- Added images:
	- images/course-1.png
	- images/course-2.png
	- images/course-3.png
	- images/course-4.png
- Modified controllers:
	- course.js
	- user.js
- Modified routes:
	- course.js
	- user.js
- Modified style.css
- Fixed Frontend JS:
	- addcourse
	- course
	- courses
	- deleteCourse
	- editCourse
	- editProfile
	- profile
	- register
	- script
- Modified index.html
- Modified HTMLS:
	- addCourse
	- course
	- courses
	- deleteCourse
	- editCourse
	- editProfile
	- login
	- logout
	- profile
	- register

Push #6:
	- Finalzied everything!

Push #7: 
	- Fixed footers!